import random
import numpy as np

from preprocessor import preprocess

from dqn_model import DQNModel
from circular_memory import CircularMemory


def q_iteration(env, gamma, model: DQNModel, sequence, episode, iteration, memory: CircularMemory, lives, no_op_steps):
    # Choose epsilon based on the iteration
    epsilon = get_epsilon_for_iteration(iteration, no_op_steps)

    # Choose the action (action is an integer representing the action index here)
    if episode < no_op_steps or random.random() < epsilon:
        action = env.action_space.sample()
    else:
        action = choose_best_action(model, sequence)

    # Take a step
    new_frame, reward, is_done, info = env.step(action)

    # Add the frame to the new sequence
    new_frame = preprocess(new_frame)
    next_sequence = np.reshape([new_frame], (1, 105, 80, 1))
    next_sequence = np.append(next_sequence, sequence[:, :, :, :3], axis=3)

    # Check if the player died (the game might not be over though, just keep the sequence unchanged)
    dead = False
    if lives > info['ale.lives']:
        dead = True

    # Add the experience to the replay memory
    memory.append((sequence, action, next_sequence, reward, dead))

    # Sample and fit
    if episode > no_op_steps:
        batch = memory.sample_batch(32)
        model.fit_batch(gamma, batch)

    if dead:
        return sequence, is_done, info['ale.lives']
    else:
        return next_sequence, is_done, info['ale.lives']


def get_epsilon_for_iteration(iteration, no_op_steps):
    # Don't take the memory population in count for the epsilon decay
    iteration = iteration - no_op_steps

    if iteration > 1000000:
        return 0.1
    else:
        return 1-(0.9*iteration/1000000)


def choose_best_action(model: DQNModel, state):
    return np.argmax(model.predict_one(state))
