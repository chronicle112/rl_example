import random


class CircularMemory:

    def __init__(self, size):
        self.data = [None] * (size + 1)
        self.start = 0
        self.end = 0

    def append(self, element):
        self.data[self.end] = element
        self.end = (self.end + 1) % len(self.data)
        if self.end == self.start:
            self.start = (self.start + 1) % len(self.data)

    def sample_batch(self, amount: int):
        batch = []
        for i in range(amount):
            batch.append(self.get_item(random.randint(0, self.size())))
        return batch

    def get_item(self, idx):
        return self.data[(self.start + idx) % self.size()]

    def size(self):
        if self.end < self.start:
            return self.end + len(self.data) - self.start
        else:
            return self.end - self.start

    def iter(self):
        for i in range(self.size()):
            yield self.get_item(i)
