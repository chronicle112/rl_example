import time

import gym


def random_play():

    # Create a breakout environment
    env = gym.make('BreakoutDeterministic-v4')
    print("Observation space shape is: {}".format(env.observation_space.shape))
    print("Number of actions is: {}".format(env.action_space.n))

    # Play 5 games with random actions
    for i in range(5):
        done = False
        env.reset()
        while not done:
            _, _, done, _ = env.step(env.action_space.sample())
            env.render()
            time.sleep(0.01)
    env.close()
