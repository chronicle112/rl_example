import keras
import numpy as np
from keras import Model
from keras.layers import Conv2D, Flatten, Dense, Lambda, Input, Multiply
from keras.models import clone_model


class DQNModel:

    def __init__(self, n_actions):
        self.weights_version = 0
        self.pixels_input_shape = (105, 80, 4)
        self.num_actions = n_actions

        # Define inputs
        frames_input = Input(self.pixels_input_shape, name='frames')
        actions_input = Input((n_actions,), name='mask')

        # Normalize image values to range [0, 1]
        normalized = Lambda(lambda x: x / 255.0)(frames_input)

        # The first hidden layer convolves 16 8×8 filters with stride 4 with the input image and applies a ReLu
        conv_1 = Conv2D(16, (8, 8), activation='relu', strides=(4, 4))(normalized)

        # The second hidden layer convolves 32 4×4 filters with stride 2 and applies a ReLu
        conv_2 = Conv2D(32, (4, 4), activation='relu', strides=(2, 2))(conv_1)

        # Flattening the second convolutional layer
        conv_flattened = Flatten()(conv_2)

        # The final hidden layer is fully-connected and consists of 256 rectifier units
        hidden = Dense(256, activation='relu')(conv_flattened)

        # The output layer is a fully-connected linear layer with a single output for each valid action
        output = Dense(n_actions)(hidden)

        # Finally, we multiply the output by the mask
        filtered_output = Multiply()([output, actions_input])

        # Create the actual model from the specified layers
        self.model = Model(inputs=[frames_input, actions_input], outputs=filtered_output)
        optimizer = keras.optimizers.RMSprop(lr=0.00025, rho=0.95, epsilon=0.01)
        self.model.compile(optimizer, loss=self.huber_loss)
        self.model.summary()
        self.target_model = clone_model(self.model)

    def huber_loss(self, y, predicted_q):
        # The Huber loss is (1/2)*a^2 for |a| <= 1 and (|a|-(1/2)) otherwise
        error = keras.backend.abs(y - predicted_q)
        quadratic_part = keras.backend.clip(error, 0.0, 1.0)
        linear_part = error - quadratic_part
        loss = keras.backend.mean(0.5 * keras.backend.square(quadratic_part) + linear_part)
        return loss

    def predict_one(self, state):
        return self.model.predict([state, np.ones(self.num_actions).reshape((1, self.num_actions))])

    def fit_batch(self, gamma, experiences):

        # Get the data from the experience replay batch
        start_states = np.zeros((len(experiences), 105, 80, 4))
        actions = np.zeros(len(experiences), dtype=int)
        next_states = np.zeros((len(experiences), 105, 80, 4))
        rewards = np.zeros(len(experiences), dtype=int)
        is_terminal = np.zeros(len(experiences), dtype=bool)

        for index, value in enumerate(experiences):
            start_states[index] = value[0]
            actions[index] = value[1]
            next_states[index] = value[2]
            rewards[index] = value[3]
            is_terminal[index] = value[4]

        actions_one_hot = np.zeros((len(experiences), self.num_actions))
        for i, action_index in enumerate(actions):
            actions_one_hot[i, action_index] = 1

        # Predict the Q values of the next states for all actions (by passing a mask of ones)
        next_q_values = self.target_model.predict([next_states, np.ones((len(experiences), self.num_actions))])

        # Set the Q values of terminal states to 0
        next_q_values[is_terminal] = 0

        # The Bellman optimality equation
        q_values = rewards + gamma * np.max(next_q_values, axis=1)

        # Fit the model, we pass the actions as a mask and multiply the targets by the actions
        self.model.fit([start_states, actions_one_hot], actions_one_hot * q_values[:, None], epochs=1, batch_size=len(start_states), verbose=0)
