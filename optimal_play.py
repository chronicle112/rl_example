import os
import random
import time

import gym
import numpy as np

from dqn_model import DQNModel
from preprocessor import preprocess


def optimal_play(version: int = -1):
    # Create a breakout environment
    env = gym.make('BreakoutDeterministic-v4')
    print("Observation space shape is: {}".format(env.observation_space.shape))
    print("Number of actions is: {}".format(env.action_space.n))

    # Recreate the DQN model
    model = DQNModel(env.action_space.n)
    load_weights(model, version)

    # Play a game with optimal actions
    env.reset()
    state, _, _, _ = env.step(1)
    state = np.stack([preprocess(state)] * 4, axis=2)
    state = np.reshape([state], (1, 105, 80, 4))

    # Keep track when the game is over
    done = False

    # Count the reward to print the final score
    sum_reward = 0

    # Count the number of lives to start the game again
    lives = 5
    while not done:
        env.render()
        time.sleep(0.02)

        # Choose the best predicted action
        optimal_action = np.argmax(model.predict_one(state))
        new_frame, reward, done, info = env.step(optimal_action)

        # Process the new frame
        new_state = preprocess(new_frame)
        new_state = np.reshape([new_state], (1, 105, 80, 1))
        new_state = np.append(new_state, state[:, :, :, :3], axis=3)

        sum_reward += reward

        if info["ale.lives"] < lives:
            # If we are dead, subtract a life, don't add the new frame and restart the game
            lives = info["ale.lives"]
            env.step(1)
        else:
            state = new_state

    print("Score: {}".format(sum_reward))

    env.close()


def load_weights(model: DQNModel, specific_version):
    files = os.listdir('weights')
    latest = -1
    if specific_version != -1:
        latest = specific_version
    else:
        for file in files:
            try:
                version = int(file[10:-3])
            except ValueError:
                version = -1
            if version > latest:
                latest = version
    if latest != -1:
        print("Loading weights from weights/iteration_{}.h5".format(latest))
        model.model.load_weights('weights/iteration_{}.h5'.format(latest))
