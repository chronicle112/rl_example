import os

import gym
import numpy as np
from circular_memory import CircularMemory
from dqn_model import DQNModel
from preprocessor import preprocess
from q_iteration import q_iteration


def deep_q_learning(num_episodes=10000):
    # Create a breakout environment
    env = gym.make('BreakoutDeterministic-v4')

    memory_size = 1000000
    memory_population_steps = 50000
    memory = CircularMemory(memory_size)
    model = DQNModel(env.action_space.n)
    save_frequency = 1000

    # Save model architecture
    create_folders()
    with open('models/model_architecture.json', 'w') as file:
        file.write(model.model.to_json())

    # Load model weights
    load_weights(model)

    # Start of memory population and training
    iteration = 0
    for episode in range(num_episodes + memory_population_steps + 1):
        if episode > memory_population_steps:
            print("Starting episode {}".format(episode-memory_population_steps))
        else:
            print("Populating memory {}/{}".format(episode, memory_population_steps))

        done = False
        state = env.reset()

        # Do nothing for a random number of steps
        for _ in range(1, 30):
            state, _, _, _ = env.step(1)

        # The player starts with 5 lives
        num_lives = 5

        # Stack the initial frame because there is no history yet
        sequence = np.stack([preprocess(state)] * 4, axis=2)
        sequence = np.reshape([sequence], (1, 105, 80, 4))

        # Iterate as long as the game hasn't finished, don't learn in the first steps to populate replay memory
        while not done:
            sequence, done, num_lives = q_iteration(env, 0.99, model, sequence, episode, iteration, memory, num_lives, no_op_steps=memory_population_steps)
            iteration = iteration + 1

            # After 10000 frame iterations, we update our target network
            if iteration % 10000 == 0:
                model.target_model.set_weights(model.model.get_weights())

        # Save model weights
        if episode > memory_population_steps and episode != 0 and episode % save_frequency == 0:
            model.weights_version = model.weights_version + save_frequency
            model.model.save_weights('weights/iteration_{}.h5'.format(model.weights_version))
            print("Saved weights for episode {} and model version {}".format(episode, model.weights_version))

    env.close()


def create_folders():
    if not os.path.isdir('models'):
        os.mkdir('models')
    if not os.path.isdir('weights'):
        os.mkdir('weights')


def load_weights(model: DQNModel):
    files = os.listdir('weights')
    latest = -1
    for file in files:
        try:
            version = int(file[10:-3])
        except ValueError:
            version = -1
        if version > latest:
            latest = version
    if latest != -1:
        print("Loading weights from weights/iteration_{}.h5".format(latest))
        model.model.load_weights('weights/iteration_{}.h5'.format(latest))
        model.weights_version = latest
    model.target_model.set_weights(model.model.get_weights())
